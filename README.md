# Currículo AltaCV

Código fonte do meu currículo em altacv.

## Detalhes Importantes

Pra usar este template no seu editor de LaTeX, garanta que:

- Você tem o arquivo altacv.cls armazenado na mesma pasta de trabalho.
- Você tem uma IDE ou editor de latex que suporte a atualização do PDF resultante (recomendo [Zathura](https://pwmt.org/projects/zathura/) pra Linux e [SumatraPDF](https://www.sumatrapdfreader.org/free-pdf-reader) pra Windows)
- Você tem o pacote [paracol](https://www.ctan.org/pkg/paracol) instalado no seu computador.
- Você tem [fontawesome5](http://texdoc.net/pkg/fontawesome5) no seu computador.

### Instalação
Copie os arquivos para uma pasta vazia, e pode começar a editar o arquivo .tex! Para compilar, pode usar `pdflatex` ou um editor integrado à sua escolha.

## Direções de Uso

Boa parte dos comandos são descritos no arquivo da classe do programa. Seguem algumas referências rápidas:

- Você pode criar seus próprios campos de cabeçalho usando `NewInfoField`;
- Os textos são afetados pelas diretivas do LaTeX (mudança de fonte, utilização de `small` ou `texttt`);
- Use `cvSection` pra criar novas seções. Consulte a biblioteca da `fontawesome5` para ícones bacanas.
- A estrutura de `cvEvent` é: {cargo}{empresa}{tempo inicial -- tempo final}{local}. Caso não queira os itens em tópicos, remova o ambiente `itemize`.
- Use `divider` para linhas bem pequenas entre trechos, e utilize `smallskip, medskip e bigskip` pra espaços manuais.
- `cvTag` pode ser usado tanto pra colocar palavras chave no contexto que coloquei, quanto em outros trechos do currículo.
- `cvAchievement` cria uma estrutura de conquista, e o uso pode ser bem criativo usando a biblioteca do `fontawesome5`!
- Sempre que abrir um ambiente com `begin{ambiente}`, não esqueça de fechar com `end{ambiente}`!
- Os links são clicáveis! Sublinhe eles usando `underline`!

## Agradecimentos
Grandes agradecimentos a [liantze](https://github.com/liantze/AltaCV) por ter feito esse modelo de currículo. É uma excelente alternativa pra quem não quer mais depender do software proprietário da Microsoft.
